<?php
	class Cliente()
	{
			// Atributos
			private $nome;
			private $telefone;
			private $dataNascimento;
			private $dataFundacao;
			private $email;
			private $cpf;
			private $cnpj;
			private $tipoCliente;
			// Construtores
			//public function cliente(){}

			public function cadastrarCliente(String $nome, int $telefone, Date $dataNascimento, Date $dataFundacao, String $email, int $cpf, int $cnpj, int $tipoCliente)
			{
				$this->nome = $nome;
				$this->telefone = $telefone;
				$this->dataNascimento = $dataNascimento;
				$this->dataFundacao= $dataFundacao;
				$this->email = $email;
				$this->cpf = $cpf;
				$this->cnpj = $cnpj;
				$this->tipoCliente = $tipoCliente;
			}

			// Métodos get's
			public function getNome()
			{
				return $this->nome;
			}

			public function getTelefone()
			{
				return $this->numero;
			}

			public function getDataNascimento()
			{
				return $this->dataNascimento;
			}

			public function getDataFundacao()
			{
				return $this->dataFundacao;
			}

			public function getEmail()
			{
				return $this->email;
				
			public function getCpf()
			{
				return $this->cpf;
				
			public function getCnpj()
			{
				return $this->cnpj;
			
			public function getTipoCliente()
			{
				return $this->tipoCliente;
			}

			// Métodos set's
			public function setNome(String $nome)
			{
				$this->nome = $nome;
			}

			public function setTelefone(int $telefone)
			{
				$this->telefone = $telefone;
			}

			public function setDataNascimento(Date $dataNascimento)
			{
				$this->dataNascimento = $dataNascimento;
			}

			public function setDataFundacao(Date $dataFundacao)
			{
				$this->dataFundacao = $dataFundacao;
			}

			public function setEmail(String $email)
			{
				$this->email = $email;
			}
			public function setCpf(int $cpf)
			{
				$this->cpf = $cpf;
			}
			public function setCnpj(int $cnpj)
			{
				$this->cnpj = $cnpj;
			}
			public function setTipoCliente(int $tipoCliente)
			{
				$this->tipoCliente = $tipoCliente;
			}
			
			
			public function alterarDadosDoCliente(String nomeNovo,int telefoneNovo,Date dataNascimentoNova, Date dataFundacaoNova, String emailNovo,int cpfNovo, int cnpjNovo)
			{
				this->nome= $nomeNovo;
				this->telefone= $telefoneNovo;
				this->dataNascimento=$dataNascimentoNova;
				this->dataFundacao = $dataFundacaoNova;
				this->email=$emailNovo;
				this->cpf= $cpfNovo;
				this->cnpj= $cnpjNovo;
				
			}
	}
?>
package DAO;

import Banco.DBConexao;
import br.com.ifm.model.Contato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;

public class ContatoDAO {

    private final String INSERT = "INSERT INTO CONTATO(nome,telefone,email) VALUES(?,?,?)";
    private final String DELETE = "DELETE FROM CONTATO where id = ?";

    public void inserir(Contato contato) {

        if (contato != null) {

            Connection conn = null;

            try {

                conn = DBConexao.getConexao();

                PreparedStatement pstm;

                pstm = conn.prepareStatement(INSERT);

                pstm.setString(1, contato.getNome());

                pstm.setString(2, contato.getTelefone());

                pstm.setString(3, contato.getEmail());

                pstm.execute();

                JOptionPane.showMessageDialog(null, "Contato cadastrado com sucesso");

                //  DBConexao.fechaConexao(conn, pstm);
            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, "Erro ao inserir contato no banco de"
                        + "dados " + e.getMessage());

            }

        } else {

            System.out.println("O contato enviado por parâmetro está vazio");

        }

    }

    public void remover(int id) {
        Connection conn = null;
        try {
            conn = DBConexao.getConexao();
            PreparedStatement pstm;
            pstm = conn.prepareStatement(DELETE);

            pstm.setInt(1, id);          
            pstm.execute();
            JOptionPane.showMessageDialog(null, "OK");
            //DBConexao.fechaConexao(conn,pstm);
  

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao excluir contato do banco de " + "dados" + e.getMessage());
        }

    }

    public static void main(String[] args) {
        Contato novo = new Contato();
        ContatoDAO cdao = new ContatoDAO();

        novo.setNome("Cris");
        novo.setTelefone("222");
        novo.setEmail("cris@gmail.com");

        //cdao.inserir(novo);
        cdao.remover(1);
    }
}
